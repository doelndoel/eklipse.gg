package com.eklipse.First_Interview;

import org.testng.annotations.DataProvider;

public class Data {
	
	@DataProvider(name = "credential-login")
	public Object[][] createData() {
		return new Object[][] {
			{"urEmail", "urPassword"}
		};
	}
}
