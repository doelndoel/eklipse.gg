package com.eklipse.First_Interview;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

public class AccountSettings extends MainFrame{
	@Test (priority =2, dataProvider="credential-login", dataProviderClass = Data.class)
	public void AccountSetting(String username, String pswd) throws IOException, InterruptedException{
		AccountStg = extentReports.createTest("Account Settings", "Make sure user can direct to account settings");
		driver.get(baseURL);
		driver.manage().window().maximize();
		AccountStg.log(Status.INFO, "Open URL : " + baseURL);
		AccountStg.log(Status.INFO, "Success to open Landing Page");
		AccountStg.log(Status.PASS, "Screenshot : " + AccountStg.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	// Click element sign in
    	WebElement elemntSignIn = driver.findElement(By.cssSelector("a.btn:nth-child(1)"));
    	elemntSignIn.click();
    	
    	//Wait login page ready to load
    	String logoStr = "h1.login-logo";
    	WaitElementPresent(logoStr);
    	AccountStg.log(Status.INFO, "Get" + logoStr);
    	AccountStg.log(Status.PASS, "Redirect to : " + AccountStg.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	//Input Username 
    	WebElement elemntUsername = driver.findElement(By.id("username"));
    	elemntUsername.sendKeys(username);
    	
    	//Input Password 
    	WebElement elemntPwd = driver.findElement(By.id("password"));
    	elemntPwd.sendKeys(pswd);
    	
    	//click btn sign in
    	WebElement clickBtnSignIn = driver.findElement(By.cssSelector("button.btn-primary"));
    	clickBtnSignIn.click();
    	AccountStg.log(Status.INFO, "Success to click button");
    	
    	Thread.sleep(2000);
    	
    	//Check if modal content showing
    	String modalShow = "body > div.fade.ek-modal-simple-premium.modal.show > div";
    	WaitElementPresent(modalShow);
    	WebElement bodyCheck = driver.findElement(By.cssSelector(modalShow));
    	boolean logDisplay = bodyCheck.isDisplayed();
    	AccountStg.log(Status.INFO, "Check Pop up Showing or Not");
    	
    	if(logDisplay) {
        	// Click element skip for now
        	WebElement elemntSkipforNow = driver.findElement(By.cssSelector("button.btn.btn-link--highlight"));
    		elemntSkipforNow.click();
    		AccountStg.log(Status.INFO, "Pop up was "+ logDisplay);
    		AccountStg.log(Status.PASS, "OnboardingPage : " + AccountStg.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	}else {
    		//Wait home page ready to use
        	String onboardingLoad = "exploration-content";
        	WaitElementPresent(onboardingLoad);
        	AccountStg.log(Status.INFO, "Pop up was "+ logDisplay);
        	AccountStg.log(Status.INFO, "Success to Login session");
        	AccountStg.log(Status.PASS, "OnboardingPage : " + AccountStg.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	}
    	
    	//click svg icon
    	WebElement clickSvgIcon = driver.findElement(By.cssSelector("li>a.nav-link>i.ic-user"));
    	clickSvgIcon.click();
    	AccountStg.log(Status.INFO, "Success to click user svg");
    	
    	//wait element present
    	String checkElementPresent = "div.show"; 
    	WaitElementPresent(checkElementPresent);
    	AccountStg.log(Status.PASS, "Success showing profile");
    	AccountStg.log(Status.PASS, "Profile pop up : " + AccountStg.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	Thread.sleep(2000);
    	
    	//click svg icon
    	WebElement btnAccStg = driver.findElement(By.cssSelector("div.show > button:nth-child(4)"));
    	btnAccStg.click();
    	AccountStg.log(Status.INFO, "Success to click Button Account Settings");
    	
    	//wait element present
    	String wordingAccstg = "div.ek-account-setting > h2";
    	WaitElementPresent(wordingAccstg);
    	AccountStg.log(Status.PASS, "Success redirect to Account Settings Page");
    	AccountStg.log(Status.PASS, "Account Settings Page : " + AccountStg.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	Thread.sleep(2000);
    	
	}	
}
