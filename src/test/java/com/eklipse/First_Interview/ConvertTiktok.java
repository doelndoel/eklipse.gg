package com.eklipse.First_Interview;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

public class ConvertTiktok extends MainFrame{
	@Test (priority =2, dataProvider="credential-login", dataProviderClass = Data.class)
	public void converts(String username, String pswd) throws IOException, InterruptedException{
		Convert = extentReports.createTest("Converts to tiktok videos", "Make sure user can convert videos");
		driver.get(baseURL);
		driver.manage().window().maximize();
		Convert.log(Status.INFO, "Open URL : " + baseURL);
		Convert.log(Status.INFO, "Success to open Landing Page");
		Convert.log(Status.PASS, "Screenshot : " + Convert.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	// Click element sign in
    	WebElement elemntSignIn = driver.findElement(By.cssSelector("a.btn:nth-child(1)"));
    	elemntSignIn.click();
    	
    	//Wait login page ready to load
    	String logoStr = "h1.login-logo";
    	WaitElementPresent(logoStr);
    	Convert.log(Status.INFO, "Get" + logoStr);
    	Convert.log(Status.PASS, "Redirect to : " + Convert.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	//Input Username 
    	WebElement elemntUsername = driver.findElement(By.id("username"));
    	elemntUsername.sendKeys(username);
    	
    	//Input Password 
    	WebElement elemntPwd = driver.findElement(By.id("password"));
    	elemntPwd.sendKeys(pswd);
    	
    	//click btn sign in
    	WebElement clickBtnSignIn = driver.findElement(By.cssSelector("button.btn-primary"));
    	clickBtnSignIn.click();
    	Convert.log(Status.INFO, "Success to click button");
    	
    	Thread.sleep(2000);
    	
    	//Check if modal content showing
    	String modalShow = "body > div.fade.ek-modal-simple-premium.modal.show > div";
    	WaitElementPresent(modalShow);
    	WebElement bodyCheck = driver.findElement(By.cssSelector(modalShow));
    	boolean logDisplay = bodyCheck.isDisplayed();
    	Convert.log(Status.INFO, "Check Pop up Showing or Not");
    	
    	if(logDisplay) {
        	// Click element skip for now
        	WebElement elemntSkipforNow = driver.findElement(By.cssSelector("button.btn.btn-link--highlight"));
    		elemntSkipforNow.click();
    		Convert.log(Status.INFO, "Pop up was "+ logDisplay);
    		Convert.log(Status.PASS, "OnboardingPage : " + Convert.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	}else {
    		//Wait home page ready to use
        	String onboardingLoad = "exploration-content";
        	WaitElementPresent(onboardingLoad);
        	Convert.log(Status.INFO, "Pop up was "+ logDisplay);
        	Convert.log(Status.INFO, "Success to Login session");
        	Convert.log(Status.PASS, "OnboardingPage : " + Convert.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	}
    	
    	//Check cookies agreement
    	String modalCookies = "cookie-notice";
    	Thread.sleep(2000);
    	WebElement cookieCheck = driver.findElement(By.id(modalCookies));
    	boolean logCookies = cookieCheck.isDisplayed();
    	Convert.log(Status.INFO, "Check Cookies");
    	
    	if(logCookies) {
    		// Click element close cookies
        	WebElement elemntClosecookies = driver.findElement(By.id("cookie-close-icon"));
        	elemntClosecookies.click();
    		Convert.log(Status.INFO, "Success close cookies : "+ logCookies);
    	}
    	
    	
    	//wait element icon edits
    	String modalEdits = "li.sidebar-item:nth-child(4) > a";
    	WaitElementPresent(modalEdits);
    	
    	//click icon edits clips
    	String modalEditsClips = "div#submenu-3 > ul:nth-child(1) > li:nth-child(1) > a.sidebar-submenu-link";
    	WaitElementPresent(modalEditsClips);
    	WebElement btnEditsClips = driver.findElement(By.cssSelector(modalEditsClips));
    	btnEditsClips.click();
    	Convert.log(Status.INFO, "Success to click Edits Clips");
    	
    	//wait element present
    	String wordingConvert = "div.flex-grow-1 > h2";
    	WaitElementPresent(wordingConvert);
    	Convert.log(Status.PASS, "Success redirect to Convert Studio");
    	Convert.log(Status.PASS, "Convert Studio Page : " + Convert.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	// click button convert
    	String waitButtonConvert = "button.empty-state__button";
    	WaitElementPresent(waitButtonConvert);
    	WebElement btnConvert = driver.findElement(By.cssSelector(waitButtonConvert));
    	btnConvert.click();
    	Convert.log(Status.INFO, "Success click button convert");
    	
    	//wait element present
    	String elementClips = "div.select-clip-empty";
    	WaitElementPresent(elementClips);
    	Convert.log(Status.PASS, "Success redirect to Select template");
    	Convert.log(Status.PASS, "Insert clip Page : " + Convert.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	// Wait for click from local
    	Thread.sleep(20000);
    	
    	// click button template
    	WebElement btntemplate = driver.findElement(By.id("template-3"));
    	btntemplate.click();
    	Convert.log(Status.INFO, "Success click template");
    	
    	//continue-editing-btn
    	WebElement btnContinueEditing = driver.findElement(By.id("continue-editing-btn"));
    	btnContinueEditing.click();
    	Convert.log(Status.INFO, "Success click continue editing");
    	
    	//next Button
    	String elementButton = "div.MuiBox-root:nth-child(4) >button.css-p2h4i0";
    	WaitElementPresent(elementButton);
    	WebElement btnNext1 = driver.findElement(By.cssSelector(elementButton));
    	btnNext1.click();
    	Convert.log(Status.INFO, "Success click Next Button 1");
    	
    	
    	//Check if button next showing
    	WaitElementPresent(elementButton);
    	WebElement buttonNextCheck = driver.findElement(By.cssSelector(elementButton));
    	boolean logButtonCheck= buttonNextCheck.isDisplayed();
    	Convert.log(Status.INFO, "Check Pop up Showing or Not");
    	
    	if(logButtonCheck) {
        	// Click element skip for now
        	WebElement elemntSkipforNow = driver.findElement(By.cssSelector(elementButton));
    		elemntSkipforNow.click();
    		Convert.log(Status.INFO, "Pop up was "+ logButtonCheck);
    		Convert.log(Status.PASS, "Confirm Page : " + Convert.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	}
    	
    	// wait element finish to load
    	Thread.sleep(10000);
    	String elementThanks = "div.MuiDialogContent-root";
    	WaitElementPresent(elementThanks);
    	Convert.log(Status.INFO, "Success to Convert Videos "+ elementThanks);
    	Convert.log(Status.PASS, "Convert Videos : " + Convert.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
	}	
}
