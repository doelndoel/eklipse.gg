package com.eklipse.First_Interview;

import java.io.IOException;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;

public class LandingPage extends MainFrame{
	
	@Test (priority=1)
	public void OpenLandingPage() throws InterruptedException, IOException {
		Landing = extentReports.createTest("Landing Page", "Make sure landing page load successfully");
		driver.get(baseURL);
		driver.manage().window().maximize();
		Landing.log(Status.INFO, "Open URL : " + baseURL);
		Landing.log(Status.INFO, "Success to open Landing Page");
    	Landing.log(Status.PASS, "Screenshot : " + Landing.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
		
		Thread.sleep(2000);
	}
}
