package com.eklipse.First_Interview;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

public class Streams extends MainFrame{
	@Test (priority =2, dataProvider="credential-login", dataProviderClass = Data.class)
	public void stream(String username, String pswd) throws IOException, InterruptedException{
		Stream = extentReports.createTest("Streams", "Make sure user can direct to stream page");
		driver.get(baseURL);
		driver.manage().window().maximize();
		Stream.log(Status.INFO, "Open URL : " + baseURL);
		Stream.log(Status.INFO, "Success to open Landing Page");
		Stream.log(Status.PASS, "Screenshot : " + Stream.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	// Click element sign in
    	WebElement elemntSignIn = driver.findElement(By.cssSelector("a.btn:nth-child(1)"));
    	elemntSignIn.click();
    	
    	//Wait login page ready to load
    	String logoStr = "h1.login-logo";
    	WaitElementPresent(logoStr);
    	Stream.log(Status.INFO, "Get" + logoStr);
    	Stream.log(Status.PASS, "Redirect to : " + Stream.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	//Input Username 
    	WebElement elemntUsername = driver.findElement(By.id("username"));
    	elemntUsername.sendKeys(username);
    	
    	//Input Password 
    	WebElement elemntPwd = driver.findElement(By.id("password"));
    	elemntPwd.sendKeys(pswd);
    	
    	//click btn sign in
    	WebElement clickBtnSignIn = driver.findElement(By.cssSelector("button.btn-primary"));
    	clickBtnSignIn.click();
    	Stream.log(Status.INFO, "Success to click button");
    	
    	Thread.sleep(2000);
    	
    	//Check if modal content showing
    	String modalShow = "body > div.fade.ek-modal-simple-premium.modal.show > div";
    	WaitElementPresent(modalShow);
    	WebElement bodyCheck = driver.findElement(By.cssSelector(modalShow));
    	boolean logDisplay = bodyCheck.isDisplayed();
    	Stream.log(Status.INFO, "Check Pop up Showing or Not");
    	
    	if(logDisplay) {
        	// Click element skip for now
        	WebElement elemntSkipforNow = driver.findElement(By.cssSelector("button.btn.btn-link--highlight"));
    		elemntSkipforNow.click();
    		Stream.log(Status.INFO, "Pop up was "+ logDisplay);
    		Stream.log(Status.PASS, "OnboardingPage : " + Stream.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	}else {
    		//Wait home page ready to use
        	String onboardingLoad = "exploration-content";
        	WaitElementPresent(onboardingLoad);
        	Stream.log(Status.INFO, "Pop up was "+ logDisplay);
        	Stream.log(Status.INFO, "Success to Login session");
        	Stream.log(Status.PASS, "OnboardingPage : " + Stream.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	}
    	
    	//click icon stream
    	WebElement btnStream = driver.findElement(By.cssSelector("div#submenu-1 >ul:nth-child(1) > li:nth-child(1) > a"));
    	btnStream.click();
    	Stream.log(Status.INFO, "Success to click Stream");
    	
    	//wait element present
    	String wordingStream = "h2.ek-page-title";
    	WaitElementPresent(wordingStream);
    	Stream.log(Status.PASS, "Success redirect to Stream Page");
    	Stream.log(Status.PASS, "Stream Page : " + Stream.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	Thread.sleep(2000);
    	
	}	
}
