package com.eklipse.First_Interview;

import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;

public class MainFrame {
  String driverPath= "C:\\webdriver\\chromedriver.exe";
  public String baseURL = "https://eklipse.gg/";
  public static WebDriver driver;
  
  //create the htmlreport object
  static ExtentSparkReporter htmlReporter;
  static ExtentReports extentReports;
  static ExtentTest Landing, Login, AccountStg, Stream, Convert;
  
  @BeforeSuite
  public void beforeSuite() {
	  // Create new one of extentReport
	  htmlReporter = new ExtentSparkReporter("extentReport.html");
	  extentReports = new ExtentReports();
	  extentReports.attachReporter(htmlReporter);
	  
	  System.out.println("Lauching Chrome Driver");
	  System.setProperty("webdriver.chrome.driver", driverPath);
	  
	  // Launching web driver
	  driver = new ChromeDriver();
  }

  @AfterSuite
  public void afterSuite() {
	  extentReports.flush();
	  System.out.println("Exit Chrome Driver");
	  driver.quit();
  }
  
  public static String TakeScreenshot() throws IOException {
		String base64 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BASE64);
		return base64;
  }
  
  public static String WaitElementPresent(String Expath){
	  (new WebDriverWait(driver,10))
		.until(ExpectedConditions.elementToBeClickable(By.cssSelector(Expath)));
	  return Expath;
  }

}
