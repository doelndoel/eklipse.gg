package com.eklipse.First_Interview;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

public class Login extends MainFrame{
	@Test (priority =2, dataProvider="credential-login", dataProviderClass = Data.class)
	public void LoginPage(String username, String pswd) throws IOException, InterruptedException{
		Login = extentReports.createTest("Login Page", "Make sure user can login safelly");
		driver.get(baseURL);
		driver.manage().window().maximize();
		Login.log(Status.INFO, "Open URL : " + baseURL);
		Login.log(Status.INFO, "Success to open Landing Page");
		Login.log(Status.PASS, "Screenshot : " + Login.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	// Click element sign in
    	WebElement elemntSignIn = driver.findElement(By.cssSelector("a.btn:nth-child(1)"));
    	elemntSignIn.click();
    	
    	//Wait login page ready to load
    	String logoStr = "h1.login-logo";
    	WaitElementPresent(logoStr);
    	Login.log(Status.INFO, "Get" + logoStr);
    	Login.log(Status.PASS, "Redirect to : " + Login.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	
    	//Input Username 
    	WebElement elemntUsername = driver.findElement(By.id("username"));
    	elemntUsername.sendKeys(username);
    	
    	//Input Password 
    	WebElement elemntPwd = driver.findElement(By.id("password"));
    	elemntPwd.sendKeys(pswd);
    	
    	//click btn sign in
    	WebElement clickBtnSignIn = driver.findElement(By.cssSelector("button.btn-primary"));
    	clickBtnSignIn.click();
    	Login.log(Status.INFO, "Success to click button");
    	
		
    	Thread.sleep(2000);
    	
    	//Check if modal content showing
    	String modalShow = "body > div.fade.ek-modal-simple-premium.modal.show > div";
    	WaitElementPresent(modalShow);
    	WebElement bodyCheck = driver.findElement(By.cssSelector(modalShow));
    	boolean logDisplay = bodyCheck.isDisplayed();
    	Login.log(Status.INFO, "Check Pop up Showing or Not");
    	
    	if(logDisplay) {
        	// Click element skip for now
        	WebElement elemntSkipforNow = driver.findElement(By.cssSelector("button.btn.btn-link--highlight"));
    		elemntSkipforNow.click();
    		Login.log(Status.INFO, "Pop up was "+ logDisplay);
    	}else {
    		//Wait home page ready to use
        	String onboardingLoad = "exploration-content";
        	WaitElementPresent(onboardingLoad);
        	Login.log(Status.INFO, "Pop up was "+ logDisplay);
        	Login.log(Status.INFO, "Success to Login session");
        	Login.log(Status.PASS, "OnboardingPage : " + Login.addScreenCaptureFromBase64String(MainFrame.TakeScreenshot()));
    	}
    	
    	Thread.sleep(2000);
    	
	}	
}
